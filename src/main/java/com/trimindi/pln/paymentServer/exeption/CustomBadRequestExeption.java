package com.trimindi.pln.paymentServer.exeption;


import com.trimindi.pln.paymentServer.utils.constanta.ResponseCode;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * Created by PC on 6/17/2017.
 */
@Provider
public class CustomBadRequestExeption implements ExceptionMapper<BadRequestException> {

    @Override
    public Response toResponse(BadRequestException e) {
        e.printStackTrace();
        return Response.status(200).entity(ResponseCode.SERVER_UNAVAILABLE).build();
    }
}
