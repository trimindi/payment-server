package com.trimindi.pln.paymentServer.repository;

import com.trimindi.pln.paymentServer.models.PartnerPIC;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by PC on 08/08/2017.
 */
@Repository
public interface PartnerPICRepository extends CrudRepository<PartnerPIC,String> {
}
