package com.trimindi.pln.paymentServer.repository;

import com.trimindi.pln.paymentServer.models.Partner;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by PC on 08/08/2017.
 */
@Repository
public interface PartnerRepository extends CrudRepository<Partner,String> {
}
