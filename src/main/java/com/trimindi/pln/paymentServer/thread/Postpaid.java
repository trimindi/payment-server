package com.trimindi.pln.paymentServer.thread;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.solab.iso8583.IsoMessage;
import com.trimindi.pln.paymentServer.AppProperties;
import com.trimindi.pln.paymentServer.PostpaidHelper;
import com.trimindi.pln.paymentServer.client.IsoMessageListener;
import com.trimindi.pln.paymentServer.client.client.Iso8583Client;
import com.trimindi.pln.paymentServer.controllers.Request;
import com.trimindi.pln.paymentServer.controllers.ReschedulableTimer;
import com.trimindi.pln.paymentServer.models.PartnerCredential;
import com.trimindi.pln.paymentServer.models.ProductItem;
import com.trimindi.pln.paymentServer.models.Transaksi;
import com.trimindi.pln.paymentServer.response.postpaid.PaymentResponse;
import com.trimindi.pln.paymentServer.response.postpaid.Rincian;
import com.trimindi.pln.paymentServer.services.deposit.PartnerDepositService;
import com.trimindi.pln.paymentServer.services.transaksi.TransaksiService;
import com.trimindi.pln.paymentServer.utils.constanta.Constanta;
import com.trimindi.pln.paymentServer.utils.constanta.ResponseCode;
import com.trimindi.pln.paymentServer.utils.constanta.TStatus;
import com.trimindi.pln.paymentServer.utils.generator.MessageGenerator;
import com.trimindi.pln.paymentServer.utils.iso.models.Rules;
import com.trimindi.pln.paymentServer.utils.iso.parsing.SDE;
import com.trimindi.pln.paymentServer.utils.rules.response.ResponseRulesGeneratorPostPaid;
import io.netty.channel.ChannelHandlerContext;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Created by PC on 05/09/2017.
 */
@Component
@Scope(value = "prototype")
public class Postpaid extends Thread implements Runnable {
    private static final Map<String, ResponseCode> failedCode;
    private static final Map<String, ResponseCode> responseCode;
    private IsoMessage inquiryRequest;
    private List<String> history = new ArrayList<>();
    static {
        Map<String, ResponseCode> aMap = new HashMap<>();
        try (InputStream stream = Thread.currentThread().getContextClassLoader().getResourceAsStream("response.properties")) {
            final Properties properties = new Properties();
            properties.load(stream);
            properties.forEach((key, value) -> {
                aMap.put(String.valueOf(key), new ResponseCode((String) key, (String) value));
            });
        } catch (IOException | NumberFormatException e) {
            throw new IllegalStateException("Unable to Response Code dictionary", e);
        }
        responseCode = Collections.unmodifiableMap(aMap);
        Map<String, ResponseCode> aMap2 = new HashMap<>();
        aMap2.put("0005", new ResponseCode("0005", responseCode.get("0005").getMessage()));
        aMap2.put("0063", new ResponseCode("0063", responseCode.get("0063").getMessage()));
        aMap2.put("0068", new ResponseCode("0068", responseCode.get("0068").getMessage()));
        failedCode = Collections.unmodifiableMap(aMap2);
    }

    private final ObjectMapper objectMapper;
    private final CloseableHttpClient client;
    private HttpPost httpPost;
    private final Iso8583Client<IsoMessage> iso8583Client;
    private ReschedulableTimer reschedulableTimer;
    private final MessageGenerator messageGenerator;
    private final AppProperties config;
    private final TransaksiService transaksiService;
    private final PartnerDepositService partnerDepositService;
    private Transaksi transaksi;
    private Logger logger = LoggerFactory.getLogger(Postpaid.class.getSimpleName());
    private IsoMessage paymentRequest;
    private Map<String, String> params;
    private ProductItem productItem;
    private PartnerCredential p;
    private AsyncResponse asyncResponse;
    private int legth;
    private String rincian;
    private String dueDate;
    private IsoMessage reversalRequest;
    private SimpleDateFormat format = new SimpleDateFormat("HH-mm-ss");

    @Autowired
    public Postpaid(Iso8583Client<IsoMessage> iso8583Client,
                    MessageGenerator messageGenerator,
                    AppProperties config,
                    TransaksiService transaksiService,
                    PartnerDepositService partnerDepositService,
                    ObjectMapper objectMapper,
                    CloseableHttpClient client) {
        this.iso8583Client = iso8583Client;
        this.messageGenerator = messageGenerator;
        this.config = config;
        this.transaksiService = transaksiService;
        this.partnerDepositService = partnerDepositService;
        this.objectMapper = objectMapper;
        this.client = client;
    }

    public void init(@Suspended AsyncResponse asyncResponse, IsoMessage inquiryRequest, PartnerCredential p, ProductItem productItem, Map<String, String> params, Request req) {
        this.inquiryRequest = inquiryRequest;
        this.productItem = productItem;
        this.params = params;
        this.asyncResponse = asyncResponse;
        this.httpPost = new HttpPost(params.get(Constanta.BACK_LINK));
        this.p = p;
        this.reschedulableTimer = new ReschedulableTimer();
        asyncResponse.setTimeout(config.timoutMessage, TimeUnit.SECONDS);
        asyncResponse.setTimeoutHandler(as -> {
            if (transaksi != null && transaksi.getST().equals(TStatus.PAYMENT_PROSESS)) {
                as.resume(Response.status(200).entity(ResponseCode.PAYMENT_UNDER_PROSES.setNtrans(transaksi.getNTRANS()).setMssidn(req.MSSIDN).setProduct(req.PRODUCT)).build());
            } else {
                as.resume(Response.status(200).entity(ResponseCode.SERVER_TIMEOUT.setMssidn(req.MSSIDN).setProduct(req.PRODUCT)).build());
            }
        });

    }

    @Override
    public void run() {
        super.run();
        try {
            /**
             * reverse postpaid
             */
            iso8583Client.addMessageListener(new IsoMessageListener<IsoMessage>() {
                @Override
                public boolean applies(IsoMessage isoMessage) {
                    return (isoMessage.getType() == 0x2410 || isoMessage.getType() == 0x2411) &&
                            isoMessage.getObjectValue(11).equals(inquiryRequest.getObjectValue(11));
                }

                @Override
                public boolean onMessage(ChannelHandlerContext ctx, IsoMessage isoMessage) {
                    if (isoMessage.getObjectValue(39).equals("0000")) {
                        reschedulableTimer.stop();
                        partnerDepositService.reverseSaldo(transaksi, isoMessage.debugString());
                        sendBack(asyncResponse,
                                Response.status(200)
                                        .entity(ResponseCode.PAYMENT_FAILED
                                                .setNtrans(transaksi.getNTRANS())
                                                .setProduct(params.get(Constanta.DENOM)).setMssidn(params.get(Constanta.MSSIDN)))
                                        .build()
                        );
                    } else {
                        if (failedCode.containsKey(isoMessage.getObjectValue(39).toString())) {
                            if (reschedulableTimer.getStep() <= 3) {
                                switch (isoMessage.getType()) {
                                    case 0x2410:
                                        reversalRequest = messageGenerator.revesalPostpaid(isoMessage, 0x2401);
                                        break;
                                    case 0x2411:
                                        reversalRequest = messageGenerator.revesalPostpaid(isoMessage, 0x2401);
                                        break;
                                }
                                if (reversalRequest != null) {
                                    ctx.writeAndFlush(reversalRequest);
                                    reschedulableTimer.reschedule();
                                } else {
                                    try {
                                        iso8583Client.connect();
                                        iso8583Client.send(reversalRequest);
                                        reschedulableTimer.reschedule();
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                }
                            } else {
                                reschedulableTimer.stop();
                                partnerDepositService.reverseSaldo(transaksi, history.toString());
                                sendBack(asyncResponse,
                                        Response.status(200)
                                                .entity(ResponseCode.PAYMENT_FAILED
                                                        .setNtrans(transaksi.getNTRANS())
                                                        .setProduct(params.get(Constanta.DENOM)).setMssidn(params.get(Constanta.MSSIDN)))
                                                .build()
                                );
                            }
                        } else {
                            reschedulableTimer.stop();
                            partnerDepositService.reverseSaldo(transaksi, history.toString());
                            sendBack(asyncResponse,
                                    Response.status(200)
                                            .entity(responseCode.get(isoMessage.getObjectValue(39).toString())
                                                    .setNtrans(transaksi.getNTRANS())
                                                    .setProduct(params.get(Constanta.DENOM)).setMssidn(params.get(Constanta.MSSIDN)))
                                            .build()
                            );
                        }
                    }
                    return false;
                }
            });
            /**
             * postpaid paymentRequest
             */
            iso8583Client.addMessageListener(new IsoMessageListener<IsoMessage>() {
                @Override
                public boolean applies(IsoMessage isoMessage) {
                    return isoMessage.getType() == 0x2210 && isoMessage.getObjectValue(11).equals(inquiryRequest.getObjectValue(11));
                }

                @Override
                public boolean onMessage(ChannelHandlerContext ctx, IsoMessage isoMessage) {
                    history.add(format.format(new Date()) + "PAYMENT RESPONSE" + isoMessage.getObjectValue(39));
                    if ("0000".equalsIgnoreCase(isoMessage.getObjectValue(39))) {
                        reschedulableTimer.stop();
                        boolean status = true;
                        List<Rules> rule = parsingRulesPostpaid(isoMessage, status);
                        com.trimindi.pln.paymentServer.response.postpaid.Payment payment = new com.trimindi.pln.paymentServer.response.postpaid.Payment(rule);
                        transaksi.setPAYMENT(isoMessage.debugString()).setTIME_PAYMENT(new Timestamp(System.currentTimeMillis()))
                                .setST(TStatus.PAYMENT_SUCCESS)
                                .setBILL_REF_NUMBER(payment.getBukopinReferenceNumber());

                        transaksiService.save(transaksi);
                        int start = 0;
                        int leghtRincian = 115;
                        double total = 0;
                        double denda = 0;
                        List<Rincian> rincians = new ArrayList<>();
                        for (int i = 0; i < payment.getBillStatus(); i++) {
                            String parsRincian = rincian.substring(start, start + leghtRincian);
                            List<Rules> rc = new SDE.Builder().setPayload(parsRincian).setRules(ResponseRulesGeneratorPostPaid.rulesRincian()).generate();
                            Rincian r = new Rincian(rc);
                            rincians.add(r);
                            dueDate = r.getDueDate();
                            total += r.getTotalElectricityBill();
                            denda += r.getPenaltyFee();
                            start += leghtRincian;
                        }
                        payment.setDenda(denda);
                        payment.setDueDate(dueDate);
                        payment.setRincians(rincians);
                        payment.setMeter(PostpaidHelper.generateStandMeter(rincians));
                        payment.setPeriod(PostpaidHelper.generatePeriode(rincians));
                        PaymentResponse baseResponse = new PaymentResponse();
                        baseResponse.setData(payment);
                        baseResponse.setFee(transaksi.getFEE());
                        baseResponse.setNtrans(transaksi.getNTRANS());
                        baseResponse.setTagihan(transaksi.getAMOUT());
                        baseResponse.setTotalBayar(transaksi.getCHARGE());
                        baseResponse.setSaldo(0);
                        baseResponse.setTotalFee(0);
                        sendBack(asyncResponse,
                                Response.status(200)
                                        .entity(baseResponse)
                                        .build()
                        );
                    } else {
                        history.add(format.format(new Date()) + "PAYMENT RESPONSE" + isoMessage.getObjectValue(39));
                        if (failedCode.containsKey(isoMessage.getObjectValue(39).toString())) {
                            if (reschedulableTimer.getStep() <= 3) {
                                reversalRequest = messageGenerator.revesalPostpaid(isoMessage, 0x2400);
                                ctx.writeAndFlush(reversalRequest);
                                reschedulableTimer.reschedule();
                            } else {
                                reschedulableTimer.stop();
                                partnerDepositService.reverseSaldo(transaksi, history.toString());
                                sendBack(asyncResponse,
                                        Response.status(200)
                                                .entity(ResponseCode.PAYMENT_FAILED.setNtrans(transaksi.getNTRANS()).setProduct(params.get(Constanta.DENOM)).setMssidn(params.get(Constanta.MSSIDN)))
                                                .build()
                                );
                            }
                        } else {
                            reschedulableTimer.stop();
                            partnerDepositService.reverseSaldo(transaksi, history.toString());
                            sendBack(asyncResponse,
                                    Response.status(200)
                                            .entity(responseCode.get(isoMessage.getObjectValue(39).toString()).setProduct(params.get(Constanta.DENOM)).setMssidn(params.get(Constanta.MSSIDN)))
                                            .build()
                            );
                        }
                    }
                    return false;
                }
            });
            /**
             * postpaid inquiry
             */
            iso8583Client.addMessageListener(new IsoMessageListener<IsoMessage>() {
                @Override
                public boolean applies(IsoMessage isoMessage) {
                    return isoMessage.getType() == 0x2110 && isoMessage.getObjectValue(2).equals(config.PAN_POSTPAID) &&
                            isoMessage.getObjectValue(11).equals(inquiryRequest.getObjectValue(11));
                }

                @Override
                public boolean onMessage(ChannelHandlerContext ctx, IsoMessage isoMessage) {
                    try {
                        history.add(format.format(new Date()) + isoMessage.getObjectValue(39));
                        if (isoMessage.getObjectValue(39).equals("0000")) {
                            boolean status = true;
                            String inquiryRes = isoMessage.getObjectValue(48);
                            List<Rules> bit48 = new SDE.Builder()
                                    .setPayload(inquiryRes)
                                    .setRules(ResponseRulesGeneratorPostPaid.postPaidInquiryResponse(48, status))
                                    .generate();
                            int legtht = new SDE.Builder()
                                    .setPayload(inquiryRes)
                                    .setRules(ResponseRulesGeneratorPostPaid.postPaidInquiryResponse(48, status))
                                    .calculate();
                            String rincian = inquiryRes.substring(legtht, inquiryRes.length());
                            com.trimindi.pln.paymentServer.response.postpaid.Inquiry inquiryResponse = new com.trimindi.pln.paymentServer.response.postpaid.Inquiry(bit48, true);
                            List<Rincian> rincians = new ArrayList<>();
                            int start = 0;
                            int leghtRincian = 115;
                            double total = 0;
                            double denda = 0;
                            for (int i = 0; i < inquiryResponse.getBillStatus(); i++) {
                                String parsRincian = rincian.substring(start, start + leghtRincian);
                                List<Rules> rc = new SDE.Builder().setPayload(parsRincian).setRules(ResponseRulesGeneratorPostPaid.rulesRincian()).generate();
                                Rincian r = new Rincian(rc);
                                rincians.add(r);
                                total += r.getTotalElectricityBill();
                                denda += r.getPenaltyFee();
                                start += leghtRincian;
                            }
                            total += denda;
                            if (Double.compare(total + inquiryResponse.getAdmin(), Double.parseDouble(params.get(Constanta.NOMINAL))) == 0) {
                                transaksi = new Transaksi()
                                        .setSTAN(isoMessage.getObjectValue(11))
                                        .setADMIN(inquiryResponse.getAdmin())
                                        .setMSSIDN_NAME(inquiryResponse.getSubscriberName())
                                        .setBILL_REF_NUMBER(inquiryResponse.getBukopinTbkReferenceNumber())
                                        .setMSSIDN(inquiryResponse.getSubscriberID())
                                        .setTIME_INQUIRY(new Timestamp(System.currentTimeMillis()))
                                        .setMERCHANT_ID(params.get(Constanta.MACHINE_TYPE))
                                        .setPRODUCT(params.get(Constanta.PRODUCT_CODE))
                                        .setAMOUT(total)
                                        .setDENOM(productItem.getDENOM())
                                        .setPRODUCT(productItem.getProduct_id())
                                        .setFEE(0)
                                        .setUSERID(p.getPartner_uid())
                                        .setINQUIRY(isoMessage.debugString())
                                        .setCHARGE(0)
                                        .setDEBET(0)
                                        .setMAC_ADDRESS(params.get(Constanta.MAC))
                                        .setIP_ADDRESS(params.get(Constanta.IP_ADDRESS))
                                        .setPARTNERID(p.getPartner_id())
                                        .setLATITUDE(params.get(Constanta.LATITUDE))
                                        .setLONGITUDE(params.get(Constanta.LONGITUDE))
                                        .setPINALTY(denda)
                                        .setST(TStatus.PAYMENT_PROSESS);
                                transaksiService.save(transaksi);
                                paymentRequest = messageGenerator.paymentPostpaid(isoMessage, transaksi);
                                ctx.writeAndFlush(paymentRequest);
                                reschedulableTimer.reschedule();
                            } else {
                                logger.error("Request {}", params.toString());
                                reschedulableTimer.stop();
                                sendBack(asyncResponse,
                                        Response.status(200)
                                                .entity(ResponseCode.NOMINAL_TIDAK_SAMA.setProduct(params.get(Constanta.DENOM)).setMssidn(params.get(Constanta.MSSIDN)))
                                                .build()
                                );
                            }
                        } else {
                            history.add(format.format(new Date()) + isoMessage.getObjectValue(39));
                            reschedulableTimer.stop();
                            sendBack(asyncResponse,
                                    Response.status(200)
                                            .entity(responseCode.get(isoMessage.getObjectValue(39).toString()).setProduct(params.get(Constanta.DENOM)).setMssidn(params.get(Constanta.MSSIDN)))
                                            .build()
                            );
                        }
                    } catch (Exception e) {
                        history.add(format.format(new Date()) + "Error ocured " + e.getLocalizedMessage());
                        reschedulableTimer.stop();
                        logger.error("Error occured {}", e.getLocalizedMessage());
                    }
                    return false;
                }
            });
            if (!iso8583Client.isConnected()) {
                iso8583Client.connectAsync();
            }
            history.add(format.format(new Date()) + inquiryRequest.debugString());
            iso8583Client.sendAsync(inquiryRequest);
            reschedulableTimer.schedule(() -> {
                if (transaksi != null && Objects.equals(transaksi.getST(), TStatus.PAYMENT_PROSESS)) {
                    try {
                        switch (reschedulableTimer.getStep()) {
                            case 0:
                                break;
                            case 1:
                                reversalRequest = messageGenerator.revesalPostpaid(paymentRequest, 0x2400);
                                iso8583Client.send(reversalRequest);
                                reschedulableTimer.reschedule();
                                break;
                            case 2:
                                reversalRequest = messageGenerator.revesalPostpaid(paymentRequest, 0x2401);
                                iso8583Client.send(reversalRequest);
                                reschedulableTimer.reschedule();
                                break;
                            case 3:
                                reversalRequest = messageGenerator.revesalPostpaid(paymentRequest, 0x2401);
                                iso8583Client.send(reversalRequest);
                                reschedulableTimer.reschedule();
                                break;
                            default:
                                reschedulableTimer.stop();
                                partnerDepositService.reverseSaldo(transaksi, reversalRequest.debugString());
                                sendBack(asyncResponse,
                                        Response.status(200)
                                                .entity(ResponseCode.PAYMENT_FAILED.setNtrans(transaksi.getNTRANS()).setProduct(params.get(Constanta.DENOM)).setMssidn(params.get(Constanta.MSSIDN)))
                                                .build()
                                );
                                break;
                        }
                    } catch (InterruptedException e) {
                        logger.error("Timmer Error {}", e.getLocalizedMessage());
                    }
                } else {
                    reschedulableTimer.stop();
                }
            });
        } catch (Exception e) {
            logger.error("Exception thrown {}", e.getLocalizedMessage());
        }
    }

    private List<Rules> parsingRulesPostpaid(IsoMessage d, boolean status) {
        try {
            List<Rules> bit48 = new SDE.Builder()
                    .setPayload(d.getObjectValue(48))
                    .setRules(ResponseRulesGeneratorPostPaid.postPaidPaymentResponse(48, status))
                    .generate();
            legth = new SDE.Builder()
                    .setPayload(d.getObjectValue(48))
                    .setRules(ResponseRulesGeneratorPostPaid.postPaidPaymentResponse(48, status))
                    .calculate();
            String inquiryRes = d.getObjectValue(48);
            rincian = inquiryRes.substring(legth, inquiryRes.length());
            if (d.hasField(63)) {
                bit48.add(new Rules(d.getObjectValue(63).toString()));
            } else {
                bit48.add(new Rules("\"Informasi Hubungi Call Center 123 Atau Hub PLN Terdekat :\""));
            }
            return bit48;
        } catch (Exception e) {
            logger.error("Parssing error {}", e.getLocalizedMessage());
        }
        return null;
    }

    protected void sendBack(@Suspended AsyncResponse response, Response r) {
        if (response.isSuspended()) {
            response.resume(r);
        } else {
            try {
                StringEntity entity = new StringEntity(objectMapper.writeValueAsString(r.getEntity()));
                httpPost.setEntity(entity);
                httpPost.setHeader("Content-type", "application/json");
                CloseableHttpResponse res;
                res = client.execute(httpPost);
                String respone = EntityUtils.toString(res.getEntity(), StandardCharsets.UTF_8.name());
                logger.error("SEND TO            -> " + "VERTICAL");
                logger.error("WITH BODY          -> " + objectMapper.writeValueAsString(r.getEntity()));
                logger.error("RESPONSE BY CLIENT -> " + respone);
            } catch (IOException e) {
                logger.error("ERROR SENDING RESPONSE " + e.getMessage());
            }
        }
    }
}
