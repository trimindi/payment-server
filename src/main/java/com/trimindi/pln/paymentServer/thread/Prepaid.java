package com.trimindi.pln.paymentServer.thread;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.solab.iso8583.IsoMessage;
import com.trimindi.pln.paymentServer.AppProperties;
import com.trimindi.pln.paymentServer.client.IsoMessageListener;
import com.trimindi.pln.paymentServer.client.client.Iso8583Client;
import com.trimindi.pln.paymentServer.controllers.Request;
import com.trimindi.pln.paymentServer.controllers.ReschedulableTimer;
import com.trimindi.pln.paymentServer.models.PartnerCredential;
import com.trimindi.pln.paymentServer.models.ProductItem;
import com.trimindi.pln.paymentServer.models.Transaksi;
import com.trimindi.pln.paymentServer.response.prepaid.Inquiry;
import com.trimindi.pln.paymentServer.response.prepaid.Payment;
import com.trimindi.pln.paymentServer.response.prepaid.PaymentResponse;
import com.trimindi.pln.paymentServer.services.deposit.PartnerDepositService;
import com.trimindi.pln.paymentServer.services.transaksi.TransaksiService;
import com.trimindi.pln.paymentServer.utils.constanta.Constanta;
import com.trimindi.pln.paymentServer.utils.constanta.ResponseCode;
import com.trimindi.pln.paymentServer.utils.constanta.TStatus;
import com.trimindi.pln.paymentServer.utils.generator.MessageGenerator;
import com.trimindi.pln.paymentServer.utils.iso.models.Rules;
import com.trimindi.pln.paymentServer.utils.iso.parsing.SDE;
import com.trimindi.pln.paymentServer.utils.rules.response.ResponseRulesGeneratorPrePaid;
import io.netty.channel.ChannelHandlerContext;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Created by PC on 05/09/2017.
 */

@Component
@Scope(value = "prototype")
public class Prepaid extends Thread implements Runnable {
    private static final Map<String, ResponseCode> failedCode;
    private static final Map<String, ResponseCode> responseCode;
    private IsoMessage inquiryRequest;

    static {
        Map<String, ResponseCode> aMap = new HashMap<>();
        try (InputStream stream = Thread.currentThread().getContextClassLoader().getResourceAsStream("response.properties")) {
            final Properties properties = new Properties();
            properties.load(stream);
            properties.forEach((key, value) -> {
                aMap.put(String.valueOf(key), new ResponseCode((String) key, (String) value));
            });
        } catch (IOException | NumberFormatException e) {
            throw new IllegalStateException("Unable to Response Code dictionary", e);
        }
        responseCode = Collections.unmodifiableMap(aMap);
        Map<String, ResponseCode> aMap2 = new HashMap<>();
        aMap2.put("0005", new ResponseCode("0005", responseCode.get("0005").getMessage()));
        aMap2.put("0063", new ResponseCode("0063", responseCode.get("0063").getMessage()));
        aMap2.put("0068", new ResponseCode("0068", responseCode.get("0068").getMessage()));
        failedCode = Collections.unmodifiableMap(aMap2);
    }

    private final ObjectMapper objectMapper;
    private final CloseableHttpClient client;
    private final AppProperties config;
    private HttpPost httpPost;
    private ReschedulableTimer reschedulableTimer;
    private final Iso8583Client<IsoMessage> iso8583Client;
    private final TransaksiService transaksiService;
    private final PartnerDepositService partnerDepositService;
    private final MessageGenerator messageGenerator;
    private Transaksi transaksi;
    private Logger logger = LoggerFactory.getLogger(Prepaid.class.getSimpleName());
    private Map<String, String> params;
    private ProductItem productItem;
    private IsoMessage paymentRequest;
    private AsyncResponse asyncResponse;
    private PartnerCredential p;

    @Autowired
    public Prepaid(AppProperties config,
                   Iso8583Client<IsoMessage> iso8583Client,
                   TransaksiService transaksiService,
                   PartnerDepositService partnerDepositService,
                   MessageGenerator messageGenerator,
                   ObjectMapper objectMapper,
                   CloseableHttpClient client) {
        this.config = config;
        this.iso8583Client = iso8583Client;
        this.transaksiService = transaksiService;
        this.partnerDepositService = partnerDepositService;
        this.messageGenerator = messageGenerator;
        this.objectMapper = objectMapper;
        this.client = client;
    }


    public void init(@Suspended AsyncResponse asyncResponse, IsoMessage inquiryRequest, PartnerCredential p, ProductItem productItem, Map<String, String> params, Request req) {
        this.inquiryRequest = inquiryRequest;
        this.productItem = productItem;
        this.params = params;
        this.asyncResponse = asyncResponse;
        this.p = p;
        this.httpPost = new HttpPost(params.get(Constanta.BACK_LINK));
        this.reschedulableTimer = new ReschedulableTimer();
        asyncResponse.setTimeout(config.timoutMessage, TimeUnit.SECONDS);
        asyncResponse.setTimeoutHandler(as -> {
            if (transaksi != null && transaksi.getST().equals(TStatus.PAYMENT_PROSESS)) {
                as.resume(Response.status(200).entity(ResponseCode.PAYMENT_UNDER_PROSES.setMssidn(req.MSSIDN).setNtrans(transaksi.getNTRANS()).setProduct(req.PRODUCT)).build());
            } else {
                as.resume(Response.status(200).entity(ResponseCode.SERVER_TIMEOUT.setMssidn(req.MSSIDN).setProduct(req.PRODUCT)).build());
            }
        });
    }

    @Override
    public void run() {
        super.run();
        try {
            /**
             * inquiry prepaid
             */
            iso8583Client.addMessageListener(new IsoMessageListener<IsoMessage>() {
                @Override
                public boolean applies(IsoMessage isoMessage) {
                    return isoMessage.getType() == 0x2110 && isoMessage.getObjectValue(2).equals(config.PAN_PREPAID) &&
                            isoMessage.getObjectValue(11).equals(inquiryRequest.getObjectValue(11));
                }

                @Override
                public boolean onMessage(ChannelHandlerContext ctx, IsoMessage isoMessage) {
                    if (isoMessage.getObjectValue(39).equals("0000")) {
                        List<Rules> bit48 = new SDE.Builder()
                                .setPayload(isoMessage.getObjectValue(48))
                                .setRules(ResponseRulesGeneratorPrePaid.prePaidInquiryResponse(48, true))
                                .generate();

                        List<Rules> bit62 = new SDE.Builder()
                                .setPayload(isoMessage.getObjectValue(62))
                                .setRules(ResponseRulesGeneratorPrePaid.prePaidInquiryResponse(62, true))
                                .generate();
                        bit48.addAll(bit62);
                        Inquiry response = new Inquiry(bit48, true);
                        if (Double.compare(productItem.getAMOUT() + response.getAdmin(), Double.parseDouble(params.get(Constanta.NOMINAL))) == 0) {
                            transaksi = new Transaksi()
                                    .setSTAN(isoMessage.getObjectValue(11))
                                    .setADMIN(response.getAdmin())
                                    .setMSSIDN_NAME(response.getSubscriberName())
                                    .setBILL_REF_NUMBER(response.getBukopinReferenceNumber())
                                    .setTIME_INQUIRY(new Timestamp(System.currentTimeMillis()))
                                    .setMSSIDN(response.getSubscriberID())
                                    .setMERCHANT_ID(params.get(Constanta.MACHINE_TYPE))
                                    .setAMOUT(productItem.getAMOUT())
                                    .setFEE(0)
                                    .setPRODUCT(productItem.getProduct_id())
                                    .setUSERID(p.getPartner_uid())
                                    .setDENOM(productItem.getDENOM())
                                    .setINQUIRY(isoMessage.debugString())
                                    .setCHARGE(0)
                                    .setDEBET(0)
                                    .setMAC_ADDRESS(params.get(Constanta.MAC))
                                    .setIP_ADDRESS(params.get(Constanta.IP_ADDRESS))
                                    .setPARTNERID(p.getPartner_id())
                                    .setLATITUDE(params.get(Constanta.LATITUDE))
                                    .setLONGITUDE(params.get(Constanta.LONGITUDE))
                                    .setPINALTY(0)
                                    .setST(TStatus.PAYMENT_PROSESS);
                            transaksiService.save(transaksi);
                            paymentRequest = messageGenerator.paymentPrepaid(transaksi, 0x2200);
                            ctx.writeAndFlush(paymentRequest);
                            reschedulableTimer.reschedule();
                        } else {
                            reschedulableTimer.stop();
                            sendBack(asyncResponse,
                                    Response.status(200)
                                            .entity(ResponseCode.NOMINAL_TIDAK_SAMA)
                                            .build()
                            );
                        }
                    } else {
                        reschedulableTimer.stop();
                        sendBack(asyncResponse, Response.status(200).entity(responseCode.get(isoMessage.getObjectValue(39).toString())).build());
                    }
                    return false;
                }
            });
            /**
             * prepaid paymentRequest
             */
            iso8583Client.addMessageListener(new IsoMessageListener<IsoMessage>() {
                @Override
                public boolean applies(IsoMessage isoMessage) {
                    return ((isoMessage.getType() == 0x2210 || isoMessage.getType() == 0x2230 || isoMessage.getType() == 0x2231) && isoMessage.getObjectValue(2).equals(config.PAN_PREPAID)) &&
                            isoMessage.getObjectValue(11).equals(inquiryRequest.getObjectValue(11));
                }

                @Override
                public boolean onMessage(ChannelHandlerContext ctx, IsoMessage isoMessage) {
                    if (isoMessage.getObjectValue(39).equals("0000")) {
                        PaymentResponse paymentResponse = transactionSuccessPrepaid(isoMessage);
                        sendBack(asyncResponse,
                                Response.status(200)
                                        .entity(paymentResponse)
                                        .build()
                        );
                    } else {
                        if (failedCode.containsKey(isoMessage.getObjectValue(39).toString())) {
                            if (reschedulableTimer.getStep() <= 3) {
                                switch (isoMessage.getType()) {
                                    case 0x2230:
                                        ctx.writeAndFlush(messageGenerator.paymentPrepaid(transaksi, 0x2220));
                                        reschedulableTimer.reschedule();
                                        break;
                                    case 0x2231:
                                        ctx.writeAndFlush(messageGenerator.paymentPrepaid(transaksi, 0x2221));
                                        reschedulableTimer.reschedule();
                                        break;
                                }
                            } else {
                                reschedulableTimer.stop();
                                partnerDepositService.reverseSaldo(transaksi, isoMessage.debugString());
                                sendBack(asyncResponse,
                                        Response.status(200)
                                                .entity(ResponseCode.PAYMENT_FAILED.setNtrans(transaksi.getNTRANS()).setProduct(params.get(Constanta.DENOM)).setMssidn(params.get(Constanta.MSSIDN)))
                                                .build()
                                );
                            }
                        } else {
                            reschedulableTimer.stop();
                            partnerDepositService.reverseSaldo(transaksi, isoMessage.debugString());
                            sendBack(asyncResponse,
                                    Response.status(200)
                                            .entity(responseCode.get(isoMessage.getObjectValue(39).toString()).setProduct(params.get(Constanta.DENOM)).setMssidn(params.get(Constanta.MSSIDN)))
                                            .build()
                            );
                        }
                    }
                    return false;
                }
            });
            if (!iso8583Client.isConnected()) {
                iso8583Client.connectAsync();
            }
            iso8583Client.sendAsync(inquiryRequest);
            reschedulableTimer.schedule(() -> {
                if (transaksi != null && Objects.equals(transaksi.getST(), TStatus.PAYMENT_PROSESS)) {
                    try {
                        switch (reschedulableTimer.getStep()) {
                            case 0:
                                break;
                            case 1:
                                iso8583Client.send(messageGenerator.paymentPrepaid(transaksi, 0x2220));
                                reschedulableTimer.reschedule();
                                break;
                            case 2:
                                iso8583Client.send(messageGenerator.paymentPrepaid(transaksi, 0x2221));
                                reschedulableTimer.reschedule();
                                break;
                            case 3:
                                iso8583Client.send(messageGenerator.paymentPrepaid(transaksi, 0x2221));
                                reschedulableTimer.reschedule();
                                break;
                            default:
                                reschedulableTimer.stop();
                                partnerDepositService.reverseSaldo(transaksi, paymentRequest.debugString());
                                sendBack(asyncResponse,
                                        Response.status(200)
                                                .entity(ResponseCode.PAYMENT_FAILED.setNtrans(transaksi.getNTRANS()).setProduct(params.get(Constanta.DENOM)).setMssidn(params.get(Constanta.MSSIDN)))
                                                .build()
                                );
                                break;
                        }
                    } catch (InterruptedException e) {
                        logger.error("Timmer error thrown {} ", e.getLocalizedMessage());
                    }
                } else {
                    reschedulableTimer.reschedule();
                }
            });
        } catch (Exception e) {
            logger.error("Exception thrown ", e.getLocalizedMessage());
        }
    }

    private PaymentResponse transactionSuccessPrepaid(IsoMessage isoMessage) {
        try {
            List<Rules> rule = parsingRulesPrepaid(isoMessage, true);
            Payment payment = new Payment(rule, true);
            transaksi.setTIME_PAYMENT(new Timestamp(System.currentTimeMillis())).setPAYMENT(isoMessage.debugString())
                    .setST(TStatus.PAYMENT_SUCCESS)
                    .setDATA(payment.getTokenNumber())
                    .setBILL_REF_NUMBER(payment.getBukopinReferenceNumber());
            transaksiService.save(transaksi);
            PaymentResponse baseResponse = new PaymentResponse();
            baseResponse.setData(payment);
            baseResponse.setNtrans(transaksi.getNTRANS());
            baseResponse.setFee(0);
            baseResponse.setSaldo(0);
            baseResponse.setTagihan(transaksi.getAMOUT());
            baseResponse.setTotalBayar(transaksi.getCHARGE());
            baseResponse.setTotalFee(0);
            return baseResponse;
        } catch (Exception e) {
            logger.error("Building reponse Error {}", e.getLocalizedMessage());
        }
        return null;
    }

    private List<Rules> parsingRulesPrepaid(IsoMessage d, boolean status) {
        try {
            List<Rules> bit48 = new SDE.Builder()
                    .setPayload(d.getObjectValue(48))
                    .setRules(ResponseRulesGeneratorPrePaid.prePaidPaymentResponse(48, status))
                    .generate();
            List<Rules> bit62 = new SDE.Builder()
                    .setPayload(d.getObjectValue(62))
                    .setRules(ResponseRulesGeneratorPrePaid.prePaidPaymentResponse(62, status))
                    .generate();
            bit48.addAll(bit62);
            if (d.hasField(63)) {
                bit48.add(new Rules(d.getObjectValue(63).toString()));
            } else {
                bit48.add(new Rules("\"Informasi Hubungi Call Center 123 Atau Hub PLN Terdekat :\""));
            }
            return bit48;
        } catch (Exception e) {
            logger.error("Parssing error {} ", e.getLocalizedMessage());
        }
        return null;
    }

    protected void sendBack(@Suspended AsyncResponse response, Response r) {
        if (response.isSuspended()) {
            response.resume(r);
        } else {
            try {
                StringEntity entity = new StringEntity(objectMapper.writeValueAsString(r.getEntity()));
                httpPost.setEntity(entity);
                httpPost.setHeader("Content-type", "application/json");
                CloseableHttpResponse res;
                res = client.execute(httpPost);
                String respone = EntityUtils.toString(res.getEntity(), StandardCharsets.UTF_8.name());
                logger.error("SEND TO            -> " + "VERTICAL");
                logger.error("WITH BODY          -> " + objectMapper.writeValueAsString(r.getEntity()));
                logger.error("RESPONSE BY CLIENT -> " + respone);
            } catch (IOException e) {
                logger.error("ERROR SENDING RESPONSE " + e.getMessage());
            }
        }
    }

}
