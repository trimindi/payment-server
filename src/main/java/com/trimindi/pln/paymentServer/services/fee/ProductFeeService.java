package com.trimindi.pln.paymentServer.services.fee;

import com.trimindi.pln.paymentServer.models.ProductFee;
import com.trimindi.pln.paymentServer.services.base.BaseService;

/**
 * Created by PC on 12/08/2017.
 */
public interface ProductFeeService extends BaseService<ProductFee, String> {
    ProductFee findFeePartner(String partner, String denom);
}
