package com.trimindi.pln.paymentServer.services.deposit;

import com.trimindi.pln.paymentServer.models.PartnerDeposit;
import com.trimindi.pln.paymentServer.models.Transaksi;
import com.trimindi.pln.paymentServer.services.base.BaseService;

/**
 * Created by PC on 12/08/2017.
 */
public interface PartnerDepositService extends BaseService<PartnerDeposit, String> {
    boolean bookingSaldo(Transaksi transaksi);

    void reverseSaldo(Transaksi transaksi, String response);
}
