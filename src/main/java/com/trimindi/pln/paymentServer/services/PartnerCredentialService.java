package com.trimindi.pln.paymentServer.services;

import com.trimindi.pln.paymentServer.models.PartnerCredential;
import com.trimindi.pln.paymentServer.services.base.BaseService;

/**
 * Created by PC on 12/08/2017.
 */
public interface PartnerCredentialService extends BaseService<PartnerCredential, String> {
}
