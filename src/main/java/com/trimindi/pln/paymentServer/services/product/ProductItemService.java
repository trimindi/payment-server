package com.trimindi.pln.paymentServer.services.product;

import com.trimindi.pln.paymentServer.models.ProductItem;
import com.trimindi.pln.paymentServer.services.base.BaseService;

/**
 * Created by PC on 12/08/2017.
 */

public interface ProductItemService extends BaseService<ProductItem, String> {
}
