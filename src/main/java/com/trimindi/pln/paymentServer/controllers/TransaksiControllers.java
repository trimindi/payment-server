package com.trimindi.pln.paymentServer.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.solab.iso8583.IsoMessage;
import com.trimindi.pln.paymentServer.AppProperties;
import com.trimindi.pln.paymentServer.PostpaidHelper;
import com.trimindi.pln.paymentServer.client.client.Iso8583Client;
import com.trimindi.pln.paymentServer.models.PartnerCredential;
import com.trimindi.pln.paymentServer.models.ProductItem;
import com.trimindi.pln.paymentServer.models.Transaksi;
import com.trimindi.pln.paymentServer.response.postpaid.Rincian;
import com.trimindi.pln.paymentServer.response.prepaid.PaymentResponse;
import com.trimindi.pln.paymentServer.services.product.ProductItemService;
import com.trimindi.pln.paymentServer.services.transaksi.TransaksiService;
import com.trimindi.pln.paymentServer.thread.Nontaglist;
import com.trimindi.pln.paymentServer.thread.Postpaid;
import com.trimindi.pln.paymentServer.thread.Prepaid;
import com.trimindi.pln.paymentServer.utils.constanta.Constanta;
import com.trimindi.pln.paymentServer.utils.constanta.ResponseCode;
import com.trimindi.pln.paymentServer.utils.constanta.TStatus;
import com.trimindi.pln.paymentServer.utils.generator.MessageGenerator;
import com.trimindi.pln.paymentServer.utils.iso.models.Rules;
import com.trimindi.pln.paymentServer.utils.iso.parsing.ParsingHelper;
import com.trimindi.pln.paymentServer.utils.iso.parsing.SDE;
import com.trimindi.pln.paymentServer.utils.rules.response.ResponseRulesGeneratorPostPaid;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.util.*;

/**
 * Created by PC on 6/17/2017.
 */
@Path("/")
@Component
@Scope(value = "request")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
public class TransaksiControllers {
    public static final Logger logger = LoggerFactory.getLogger(TransaksiControllers.class);
    private static final Map<String, ResponseCode> failedCode;
    private static final Map<String, ResponseCode> responseCode;

    static {
        Map<String, ResponseCode> aMap = new HashMap<>();
        try (InputStream stream = Thread.currentThread().getContextClassLoader().getResourceAsStream("response.properties")) {
            final Properties properties = new Properties();
            properties.load(stream);
            properties.forEach((key, value) -> {
                aMap.put(String.valueOf(key), new ResponseCode((String) key, (String) value));
            });
        } catch (IOException | NumberFormatException e) {
            throw new IllegalStateException("Unable to Response Code dictionary", e);
        }
        responseCode = Collections.unmodifiableMap(aMap);
        Map<String, ResponseCode> aMap2 = new HashMap<>();
        aMap2.put("0005", new ResponseCode("0005", responseCode.get("0005").getMessage()));
        aMap2.put("0063", new ResponseCode("0063", responseCode.get("0063").getMessage()));
        aMap2.put("0068", new ResponseCode("0068", responseCode.get("0068").getMessage()));
        failedCode = Collections.unmodifiableMap(aMap2);
    }

    private final
    MessageGenerator messageGenerator;
    private final
    ProductItemService productItemService;
    private final Iso8583Client<IsoMessage> isoMessageIso8583Client;
    private final AppProperties config;
    private final TransaksiService transaksiService;
    private final ObjectMapper objectMapper;
    private final CloseableHttpClient client;
    final
    Prepaid prepaid;
    final
    Nontaglist nontaglist;
    final
    Postpaid postpaid;
    private IsoMessage inquiryRequest;
    private Map<String, String> params = new HashMap<>();
    private ProductItem productItem;
    private Transaksi transaksi;
    private HttpPost httpPost;

    @Autowired
    public TransaksiControllers(MessageGenerator messageGenerator, ProductItemService productItemService, Iso8583Client<IsoMessage> isoMessageIso8583Client, AppProperties config, TransaksiService transaksiService, ObjectMapper objectMapper, CloseableHttpClient client, Prepaid prepaid, Nontaglist nontaglist, Postpaid postpaid) {
        this.messageGenerator = messageGenerator;
        this.productItemService = productItemService;
        this.isoMessageIso8583Client = isoMessageIso8583Client;
        this.config = config;
        this.transaksiService = transaksiService;
        this.objectMapper = objectMapper;
        this.client = client;
        this.prepaid = prepaid;
        this.nontaglist = nontaglist;
        this.postpaid = postpaid;
    }

    @POST
    public void inquiry(@Suspended AsyncResponse asyncResponse,
                        @Context ContainerRequestContext security,
                        @Valid Request req) {
        PartnerCredential p = (PartnerCredential) security.getProperty(Constanta.PRINCIPAL);
        httpPost = new HttpPost(p.getBack_link());
        params.put(Constanta.MACHINE_TYPE, p.getMerchant_type());
        params.put(Constanta.MSSIDN, req.MSSIDN);
        params.put(Constanta.AREA, req.AREA);
        params.put(Constanta.TID, req.TID);
        params.put(Constanta.IP_ADDRESS, String.valueOf(security.getProperty(Constanta.IP_ADDRESS)));
        params.put(Constanta.MAC, String.valueOf(security.getProperty(Constanta.MAC)));
        params.put(Constanta.LONGITUDE, String.valueOf(security.getProperty(Constanta.LONGITUDE)));
        params.put(Constanta.LATITUDE, String.valueOf(security.getProperty(Constanta.LATITUDE)));
        params.put(Constanta.DENOM, req.PRODUCT);
        params.put(Constanta.PID, req.PID);
        params.put(Constanta.NOMINAL, req.NOMINAL);
        params.put(Constanta.BACK_LINK, p.getBack_link());

        switch (req.ACTION) {
            case "PLN.PAYMENT.INSTANSI":
                productItem = productItemService.findById(req.PRODUCT);
                if (productItem == null) {
                    sendBack(asyncResponse, Response.status(200).entity(ResponseCode.PARAMETER_UNKNOW_DENOM.setMssidn(req.MSSIDN).setProduct(req.PRODUCT)).build());
                    return;
                }
                if (!productItem.getFk_biller().equalsIgnoreCase("001")) {
                    sendBack(asyncResponse, Response.status(200).entity(ResponseCode.PARAMETER_INVALID_PRODUCT_ACTION.setMssidn(req.MSSIDN).setProduct(req.PRODUCT)).build());
                    return;
                }
                if (!productItem.isACTIVE()) {
                    sendBack(asyncResponse, Response.status(200).entity(ResponseCode.PRODUCT_UNDER_MAINTANCE.setMssidn(req.MSSIDN).setProduct(req.PRODUCT)).build());
                    return;
                }
                switch (productItem.getProduct_id()) {
                    case "99501":
                        inquiryRequest = messageGenerator.inquiryPostpaid(req.MSSIDN, params.get(Constanta.MACHINE_TYPE), req.TID, req.PID);
                        postpaid.init(asyncResponse, inquiryRequest, p, productItem, params, req);
                        postpaid.run();
                        break;
                    case "99502":
                        inquiryRequest = messageGenerator.inquiryPrepaid(params.get(Constanta.MACHINE_TYPE), req.MSSIDN, req.TID, req.PID);
                        prepaid.init(asyncResponse, inquiryRequest, p, productItem, params, req);
                        prepaid.run();
                        break;
                    case "99504":
                        inquiryRequest = messageGenerator.inquiryNontaglist(req.MSSIDN, params.get(Constanta.MACHINE_TYPE), req.TID, req.PID);
                        nontaglist.init(asyncResponse, inquiryRequest, p, productItem, params, req);
                        nontaglist.run();
                        break;
                    default:
                        asyncResponse.resume(Response.status(200).entity(ResponseCode.UNKNOW_ACTION.setProduct(req.PRODUCT).setMssidn(req.MSSIDN)).build());
                        break;
                }
                break;
            case "PLN.CETAK.ULANG":
                cetakStruk(asyncResponse, req);
                break;
            default:
                asyncResponse.resume(Response.status(200).entity(ResponseCode.UNKNOW_ACTION.setProduct(req.PRODUCT).setMssidn(req.MSSIDN)).build());
                break;
        }
    }

    private void sendBack(@Suspended AsyncResponse response,Response r){
        if(response.isSuspended()){
            response.resume(r);
        } else {
            try {
                StringEntity entity = new StringEntity(objectMapper.writeValueAsString(r.getEntity()));
                httpPost.setEntity(entity);
                httpPost.setHeader("Accept", "application/json");
                httpPost.setHeader("Content-type", "application/json");
                CloseableHttpResponse res;
                res = client.execute(httpPost);
                String respone = EntityUtils.toString(res.getEntity(), StandardCharsets.UTF_8.name());
                logger.error("SEND TO            -> " + params.get(Constanta.BACK_LINK));
                logger.error("WITH BODY          -> " + objectMapper.writeValueAsString(r.getEntity()));
                logger.error("RESPONSE BY CLIENT -> " + respone);
            } catch (IOException e) {
                logger.error("ERROR SENDING RESPONSE " + e.getMessage());
                e.printStackTrace();
            }
        }
    }


    private void cetakStruk(@Suspended final AsyncResponse asyncResponse, Request req) {
        transaksi = transaksiService.findById(req.NTRANS);
        if (transaksi != null && Objects.equals(transaksi.getST(), TStatus.PAYMENT_SUCCESS)) {
            if (transaksi.getPRT() >= 3) {
                asyncResponse.resume(Response.status(200).entity(ResponseCode.MAXIMUM_PRINTED_RECIPT.setProduct(transaksi.getDENOM()).setMssidn(transaksi.getMSSIDN())).build());
            } else {
                IsoMessage isoMsg = null;
                try {
                    isoMsg = isoMessageIso8583Client.getIsoMessageFactory().parseMessage(transaksi.getPAYMENT().getBytes(), 0);
                } catch (ParseException | UnsupportedEncodingException e) {
                    asyncResponse.resume(Response.status(200).entity(ResponseCode.SERVER_UNAVAILABLE.setProduct(transaksi.getPRODUCT()).setMssidn(req.MSSIDN)).build());
                }

                List<Rules> rules;
                switch (transaksi.getPRODUCT()) {
                    case "99501":
                        rules = ParsingHelper.parsingRulesPostPaid(isoMsg, true);
                        com.trimindi.pln.paymentServer.response.postpaid.Payment payment = new com.trimindi.pln.paymentServer.response.postpaid.Payment(rules);
                        String bit48 = isoMsg.getObjectValue(48);
                        int legth = new SDE.Builder().setPayload(bit48).setRules(ResponseRulesGeneratorPostPaid.postPaidPaymentResponse(48, true)).calculate();
                        String rincian = bit48.substring(legth, bit48.length());
                        int start = 0;
                        int leghtRincian = 115;
                        List<Rincian> rincians = new ArrayList<>();
                        for (int i = 0; i < payment.getBillStatus(); i++) {
                            String parsRincian = rincian.substring(start, start + leghtRincian);
                            List<Rules> rc = new SDE.Builder().setPayload(parsRincian).setRules(ResponseRulesGeneratorPostPaid.rulesRincian()).generate();
                            Rincian r = new Rincian(rc);
                            rincians.add(r);
                            start += leghtRincian;
                        }
                        payment.setPeriod(PostpaidHelper.generatePeriode(rincians));
                        payment.setMeter(PostpaidHelper.generateStandMeter(rincians));
                        com.trimindi.pln.paymentServer.response.postpaid.PaymentResponse baseResponse = new com.trimindi.pln.paymentServer.response.postpaid.PaymentResponse();
                        payment.setRincians(rincians);
                        baseResponse.setData(payment);
                        baseResponse.setFee(transaksi.getFEE());
                        baseResponse.setNtrans(transaksi.getNTRANS());
                        baseResponse.setTagihan(transaksi.getAMOUT());
                        baseResponse.setTotalBayar(transaksi.getCHARGE());
                        baseResponse.setSaldo(0);
                        baseResponse.setTotalFee(0);
                        transaksi.setPRT(transaksi.getPRT() + 1);
                        transaksiService.update(transaksi);
                        asyncResponse.resume(Response.status(200).entity(baseResponse).build());
                        break;
                    case "99502":
                        rules = ParsingHelper.parsingRulesPrepaid(isoMsg, true);
                        com.trimindi.pln.paymentServer.response.prepaid.Payment prepaid = new com.trimindi.pln.paymentServer.response.prepaid.Payment(rules, true);
                        PaymentResponse baseResponse1 = new PaymentResponse();
                        baseResponse1.setData(prepaid);
                        baseResponse1.setNtrans(transaksi.getNTRANS());
                        baseResponse1.setFee(0);
                        baseResponse1.setSaldo(0);
                        baseResponse1.setTagihan(transaksi.getAMOUT());
                        baseResponse1.setTotalBayar(transaksi.getCHARGE());
                        baseResponse1.setTotalFee(0);
                        transaksi.setPRT(transaksi.getPRT() + 1);
                        transaksiService.update(transaksi);
                        asyncResponse.resume(Response.status(200).entity(baseResponse1).build());
                        break;
                    case "99504":
                        rules = ParsingHelper.parsingRulesNontaglist(isoMsg, true);
                        com.trimindi.pln.paymentServer.response.nontaglist.Payment nontaglist = new com.trimindi.pln.paymentServer.response.nontaglist.Payment(rules);
                        com.trimindi.pln.paymentServer.response.nontaglist.PaymentResponse baseResponse2 = new com.trimindi.pln.paymentServer.response.nontaglist.PaymentResponse();
                        baseResponse2.setFee(transaksi.getFEE());
                        baseResponse2.setSaldo(0);
                        baseResponse2.setTotalFee(0);
                        baseResponse2.setTagihan(transaksi.getAMOUT());
                        baseResponse2.setTotalBayar(transaksi.getCHARGE());
                        baseResponse2.setNtrans(transaksi.getNTRANS());
                        baseResponse2.setData(nontaglist);
                        transaksi.setPRT(transaksi.getPRT() + 1);
                        transaksiService.update(transaksi);
                        asyncResponse.resume(Response.status(200).entity(baseResponse2).build());
                }
            }
        } else {
            asyncResponse.resume(Response.status(200).entity(ResponseCode.NTRANS_NOT_FOUND).build());
        }
    }
}

