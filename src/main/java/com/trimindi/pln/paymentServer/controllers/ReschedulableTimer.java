package com.trimindi.pln.paymentServer.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by PC on 03/09/2017.
 */
//@Component
//@Scope(value = "prototype")
public class ReschedulableTimer extends Timer {
    public static final Logger logger = LoggerFactory.getLogger(ReschedulableTimer.class);
    public boolean stop = false;
    private Runnable task;
    private TimerTask timerTask;
    private Timer timer;
    private long delay = 45000;
    private int step = 0;

    public ReschedulableTimer() {
    }

    public void schedule(Runnable runnable) {
        task = runnable;
        timer = new Timer();
        timerTask = new TimerTask() {
            public void run() {
                task.run();
            }
        };
        timer.schedule(timerTask, delay);
    }

    public void reschedule() {
        step++;
        if (!stop) {
            timerTask.cancel();
            timer = new Timer();
            timerTask = new TimerTask() {
                public void run() {
                    task.run();
                }
            };
            timer.schedule(timerTask, delay);
        }
        if (step > 4) {
            stop();
        }
    }

    public void stop() {
        stop = true;
        timer.cancel();
        timerTask.cancel();
    }

    public int getStep() {
        return step;
    }

    public ReschedulableTimer setStep(int step) {
        this.step = step;
        return this;
    }
}
